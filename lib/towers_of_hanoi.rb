# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  @towers
  @disk_count

  attr_reader :towers, :disk_count

  def initialize
    disks = *3.downto(1)
    @towers = [disks, [], []]
    @disk_count = disks.length
  end

  def play
    start_render
    while !won?
      render
      # validate input
      valid_input = false
      while valid_input == false
        from_tower, to_tower = get_input
        if valid_input?(from_tower, to_tower)
          if valid_move?(from_tower, to_tower)
            valid_input = true
          end
        end
      end
      # move disks
      move(from_tower, to_tower)
      # check for win conditions
      won?
    end
    puts "Congratulations, you won!"
  end

  def won?
    disks = *@disk_count.downto(1)
    true if towers[2] == disks || towers[1] == disks
  end

  def render
    # render tower in text format
    @towers.each do |tower|
      print "Tower #{towers.index(tower)}'s disks: "
      tower.each do |disk|
        print "#{disk} "
      end
      puts ""
    end
  end

  def start_render
    puts "####################################"
    puts "#                                  #"
    puts "#              Towers              #"
    puts "#                                  #"
    puts "#                Of                #"
    puts "#                                  #"
    puts "#              Hanoi!              #"
    puts "#                                  #"
    puts "####################################"
    puts
  end

  def get_input
    print "\nTake disk from tower:"
    from_tower = gets.chomp
    print "Put disk on tower:"
    to_tower = gets.chomp
    [from_tower.to_i, to_tower.to_i]
  end

  def valid_move?(from_tower, to_tower)
    # assuming technically correct inputs, check for game validity

    # can't take disk from empty tower
    if @towers[from_tower].empty?
      puts "Error: Cannot lift a disk from an empty tower."
      false
    # can't place big disks on smaller ones
    elsif !@towers[to_tower].empty? &&
    @towers[from_tower].last > @towers[to_tower].last
      puts "Error: Cannot place a disk on a smaller one."
      false
    # can't move disk onto same tower
    elsif from_tower == to_tower
      puts "Error: Cannot put a disk on the tower it was just on."
      false
    else
      true
    end
  end

  def valid_input?(from_tower, to_tower)
    # check if inputs are in range [1, 3]
    if from_tower.to_i > 2 || from_tower.to_i < 0 ||
    to_tower.to_i < 0 || to_tower.to_i > 2
      puts "Error: Integer should be 0, 1, or 2. Please try again."
      false
    else
      true
    end
  end

  def move(from_tower, to_tower)
    disk = @towers[from_tower].pop
    @towers[to_tower].push(disk)
  end
end
